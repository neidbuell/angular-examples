/**
 * Created by neid on 11/16/16.
 */
(function() {
    "use strict";

    angular.module('app').directive('book', book);

    book.$inject = ['$http', '$templateCache', '$compile', '$parse'];

    function book($http, $templateCache, $compile, $parse) {
        return {
            scope: {
                data: '='
            },
            controller: function($scope) {
                $scope.big = 'big';
                $scope.small = 'small';
            },
            restrict: 'E',
            link : function(scope, element, attrs) {
                var template = 'templates/' + $parse(attrs.data)(scope) + '.html';
                $http.get(template, {cache: $templateCache})
                    .success(function(templateContent) {
                        element.replaceWith($compile(templateContent)(scope));
                    })
            }
        }
    }

})();